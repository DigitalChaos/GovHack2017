//These values were given to create a restangle view over the top of 
// Afghanistan
var west = 62.0;
var south = 31.0;
var east = 64.0;
var north = 33.0;

//Cameara will move to this location at the beginning
var rectangle = Cesium.Rectangle.fromDegrees(west, south, east, north);
Cesium.BingMapsApi.defaultKey = 'AmobPhnPOffAkgAVdYcMdf9hZUdO5bfEztQRH10f-ho_jUUlypfzneo4EII58QnB';
Cesium.Camera.DEFAULT_VIEW_FACTOR = 0.5;
Cesium.Camera.DEFAULT_VIEW_RECTANGLE = rectangle;
//Cesium.Camara.defaultZoomAmount = 20;
// NOTE: Viewer constructed after default view is set.
//Remember to swap this around
var cesiumWidget =  new Cesium.Viewer('cesiumContainer', {
    terrainProviderViewModels : [], //Disable terrain changing
    infoBox : true, //Disable InfoBox widget
    selectionIndicator : true //Disable selection indicator
    
});

//sets default map
var baseLayerPickerViewModel = cesiumWidget.baseLayerPicker.viewModel;
baseLayerPickerViewModel.selectedImagery = baseLayerPickerViewModel.imageryProviderViewModels[1];

//Stops double click zoom in on entities
cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK)
//Add Cesium Inspector
//cesiumWidget.extend(Cesium.viewerCesiumInspectorMixin);
/*function onChanged(collection, added, removed, changed){
    
    var msg = 'Added ids';
    for(var i = 0; i < added.length; i++){
        msg += '\n' + added[i].id;
        
    }
}    
    
cesiumWidget.entities.collectionChanged.addEventListener(onChanged());
*/
var pointsList = [];
var polygonsList = [];
var startDateForPoints = new Date(2004, 0, 1); 
var endDateForPoints = new Date(2013, 0, 1); 
var addedCategoriesFilt = [];

function monthDiffApp(d1, d2) {
    return d2.getMonth() - d1.getMonth() + (12 * (d2.getFullYear() - d1.getFullYear()));
}


function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return new Cesium.Color((parseInt(result[1], 16)/255.0), (parseInt(result[2], 16)/255.0), (parseInt(result[3], 16)/255.0), 1);

}

function loadData() {
    var fadingStartDate = 5;
    var colorsWithCategory = []; // = colordata;
    for(var i = 0; i < monthDiffApp(startDateForPoints, endDateForPoints); i++)   
        polygonsList.push(new Array());
 
    for(var i = 0; i < monthDiffApp(startDateForPoints, endDateForPoints); i++)   
            pointsList.push(new Array());   
            
    //var colours = {};
    var noOfDaysShown = 25; 
    /*for (var ckey in visdata.colours) {
        var c = visdata.colours[ckey];
        colours[ckey] = new Cesium.Color(c.r, c.g, c.b, 1);
    }*/
    //visdata.frames.length
    for (var i = 0; i < visdata.frames.length; i++) {
        var frame = visdata.frames[i];

        var start = Cesium.JulianDate.fromIso8601(frame.start_date);
        var stop = Cesium.JulianDate.fromIso8601(frame.end_date);
        var j = 0;

        if (frame.hasOwnProperty('points')) {
            for (j = 0; j < frame.points.length; j++) {
                var p = frame.points[j];
                var newColor = true; 
                var sub;
                var cat;
                var colourEntity;
                sub =p["subcategory"];
                cat = p["category"];
        
                if ((typeof sub != undefined) && sub != "undefined" && sub != null && sub !== undefined){
                } else {
                    sub = "NA";
                }
                var isfound = false;
                //Checks if category and colour have already been installed
                for(var h = 0; h < colorsWithCategory.length; h++){
                    if(colorsWithCategory[h]["category"] == cat && colorsWithCategory[h]["subcategory"] == sub)
                    {
                        isfound = true;
                        colourEntity = colorsWithCategory[h]["color"];
                        break;
                    }
                }
                //If it has not been added then add and set point to colour
                if(isfound == false)
                {
                    for(var l = 0; l < colordata.length; l++){
                        if(cat == colordata[l].category &&  sub == colordata[l].subcategory){
                            isfound = true;
                            colourEntity = hexToRgb(colordata[l].color);
                            for(var h = 0; h < colorsWithCategory.length; h++){
                                if(colorsWithCategory[h]["category"] == colordata[l].category && colorsWithCategory[h]["subcategory"] == colordata[l].subcategory)
                                {
                                    newColor = false;
                                    break;
                                }
                            }
                            
                            if(newColor == true)
                            {
                                var newColorCat = {color : colourEntity, category : colordata[l].category, subcategory : colordata[l].subcategory};
                                colorsWithCategory.push(newColorCat);
                            }
                            break;   
                        }
                        if(isfound)
                            break;
                    }
                }

                var point = new Cesium.Entity(
                {
                    name: cat + "-" + sub, 
                    position : Cesium.Cartesian3.fromDegrees(p.long, p.lat),
                    availability : new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                        start : start,
                        stop : stop
                    })]),
                    point : {
                        pixelSize : 5,
                        color : colourEntity,
                        outlineColor : colourEntity,
                        outlineWidth : 2
                    }
                });
                
                var date = start-stop;
                var pointWithDay = {date: date ,point:point, startDate: start, endDate: stop, category: cat, subcategory : sub};
                var ds = new Date(start);
                ds.setDate(ds.getDate() - fadingStartDate);
                pointsList[Math.abs(monthDiffApp(startDateForPoints,ds ))].push(pointWithDay); 
            }
        }
        if(prediction == true)
        {
            for(var m = 0; m < pointsList.length; m++)
            {
                for(var k = 0; k< pointsList[m].length; k+=2)
                {
                    //console.log(pointsList[m][k].startDate + " " + pointsList[m][k].stopDate);
                    cesiumWidget.entities.add({
                        name : 'Glowing blue line on the surface',
                        availability : new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                            start : pointsList[m][k].startDate,
                            stop : pointsList[m][k].endDate
                            })]),
                            
                        polyline : {
                            
                            positions : [pointsList[m][k+1].point.position.getValue(pointsList[m][k+1].startDate),pointsList[m][k].point.position.getValue(pointsList[m][k].startDate)],
                            width : 5,
                            followSurface : true,
                            material : new Cesium.PolylineGlowMaterialProperty({
                                color : Cesium.Color.BLUE
                            })
                        }
                    });
                }
            }
        }
        //console.log(visdata.frames.length);
        if (frame.hasOwnProperty('polygons')) {
            for (j = 0; j < frame.polygons.length; j++) {
                //frame.polygons.length
                
                //Create a new instance.
                var convexHull = new ConvexHullGrahamScan();
                var newPolyArray = [];
                var k;
                var l;
                var poly = frame.polygons[j];
                var newColor = true; 
                var sub;
                var cat;
                var colourEntity;
                sub =poly["subcategory"];
                cat = poly["category"];
                
                if ((typeof sub != undefined) && sub != "undefined" && sub != null && sub !== undefined){
                } else {
                    sub = "NA";
                }
                
                for(k = 0; k < poly.points.length; k+= 2)
                {
                    //add points
                    //(needs to be done for each point, a foreach
                    //loop on the input array can be used.)
                    convexHull.addPoint(poly.points[k], poly.points[k+1]);
                    
                }
                //getHull() returns the array of points
                //that make up the convex hull.
                var hullPoints = convexHull.getHull();
                for(k = 0; k < hullPoints.length; k++)
                {
                    for(l = 0; l < poly.points.length; l+=2)
                    {
                        if(poly.points[l] == hullPoints[k].x && poly.points[l+1] == hullPoints[k].y)
                        {
                            newPolyArray.push(poly.points[l]);
                            newPolyArray.push(poly.points[l+1]);
                        }
                    }
                }
                
                    
                var isfound = false;
                 //Checks if category and colour have already been installed
                for(var h = 0; h < colorsWithCategory.length; h++){
                    if(colorsWithCategory[h]["category"] == cat && colorsWithCategory[h]["subcategory"] == sub)
                    {
                        isfound = true;
                        colourEntity = colorsWithCategory[h]["color"];
                        break;
                    }
                }
                //If it has not been added then add and set point to colour
                if(isfound == false)
                {
                    for(var l = 0; l < colordata.length; l++){
                        if(cat == colordata[l].category &&  sub == colordata[l].subcategory){
                            isfound = true;
                            colourEntity = hexToRgb(colordata[l].color);
                            //console.log("Colour " + colourEntity);
                            for(var h = 0; h < colorsWithCategory.length; h++){
                                if(colorsWithCategory[h]["category"] == colordata[l].category && colorsWithCategory[h]["subcategory"] == colordata[l].subcategory)
                                {
                                    newColor = false;
                                    break;
                                }
                            }
                            
                            if(newColor == true)
                            {
                                var newColorCat = {color : colourEntity, category : colordata[l].category, subcategory : colordata[l].subcategory};
                                colorsWithCategory.push(newColorCat);
                            }
                            break;   
                        }
                        if(isfound)
                            break;
                    }    
                }   
                    
                var polyCes = new Cesium.Entity({
                    name: cat + "-" + sub,
                    availability: new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                        start: Cesium.JulianDate.addDays(start,-noOfDaysShown,new Cesium.JulianDate()),
                        stop: Cesium.JulianDate.addDays(stop,noOfDaysShown,new Cesium.JulianDate())
                    })]),
                    polygon: new Cesium.PolygonGraphics({
                        hierarchy: Cesium.Cartesian3.fromDegreesArray(newPolyArray),
                        material: colourEntity,
                        outlineColor: colourEntity
                    })
                    
                });

                var polyWithDay = {polygon:polyCes, polyColor: colourEntity, start: start, stop: stop, startDate: Cesium.JulianDate.addDays(start,-30,new Cesium.JulianDate()), endDate: Cesium.JulianDate.addDays(stop, 30, new Cesium.JulianDate()), category : cat, subcategory: sub};
                var ds = new Date(frame.start_date);
                ds.setDate(ds.getDate() - noOfDaysShown);
                polygonsList[Math.abs(monthDiffApp(startDateForPoints,ds ))].push(polyWithDay);  
                //cesiumWidget.entities.add(polyCes);
            }
        }
    }
    
      $(document).ready(function(){

        var t = $('#KeyTable').DataTable();
        addedCategoriesFilt.push({Category : 'none', SubCategory : ["none"] });
        for(var k = 0; k < colorsWithCategory.length; k++)
        {
            t.row.add( [
            colorsWithCategory[k]["category"],
            colorsWithCategory[k]["subcategory"],
            "<div style='background-color:rgb("+colorsWithCategory[k]["color"]["red"]*100 +"%,"+colorsWithCategory[k]["color"]["green"] * 100 +"%,"+colorsWithCategory[k]["color"]["blue"] * 100 +"%) '>_</div>"
        ] ).draw( false );
            //document.getElementById("catSelect").innerHTML += "<option Value=" + colorsWithCategory[k]["category"] + ">" + colorsWithCategory[k]["category"] + "</option>";    
            //document.getElementById("subcatSelect").innerHTML += "<option Value=" + colorsWithCategory[k]["subcategory"] + ">" + colorsWithCategory[k]["subcategory"] + "</option>";    
            var AddedCat = false;
            for(var l = 0; l < addedCategoriesFilt.length; l++)
            {
                if(addedCategoriesFilt[l]['Category'].includes(colorsWithCategory[k]["category"]))
                {
                    AddedCat = true;
                    var AddedSub = false;
                    for(var r = 0; r < addedCategoriesFilt[l]['SubCategory'].length; r++)
                    {
                        if(addedCategoriesFilt[l]['SubCategory'][r].includes(colorsWithCategory[k]["subcategory"]) )
                        {
                            AddedSub = true;
                            break;
                        }
                    }
                    if(AddedSub == false)
                    {
                        addedCategoriesFilt[l]['SubCategory'].push(colorsWithCategory[k]["subcategory"] );
                        var id = "cat-" +  l + "-sub-" + (addedCategoriesFilt[l]['SubCategory'].length - 1);
                        var ParentId =  "cat-" +  l + "-ul";
                        document.getElementById(ParentId).innerHTML += "<li><input type='checkbox' name=" + colorsWithCategory[k]["subcategory"] + " id='" + id + "'/><label for='" + id + "'>" +  colorsWithCategory[k]["subcategory"] +"</label></li>";
                        break;
                    }
                }
            }
            if(AddedCat == false)
            {
                var item = { Category : '', SubCategory : []};
                item['Category'] = colorsWithCategory[k]["category"];
                item['SubCategory'] = [(colorsWithCategory[k]["subcategory"] )];
                addedCategoriesFilt.push(item);
                var ParentId =  "cat-" +  l;
                var ChildId = "cat-" +  l + "-sub-0"; 
                var ParentIdUl =  "cat-" +  l + "-ul";
                document.getElementById("checkBoxDisplay").innerHTML += "<li> <input type='checkbox' name=" + colorsWithCategory[k]["category"] + " id='" + ParentId + "'/><label for='" + id + "'>" +  colorsWithCategory[k]["category"] +"</label>"+" <button id='" + ParentIdUl + 'But'  + "' onclick='toggle(\"#" + ParentIdUl + "\", \"#" + ParentIdUl + 'But'  + "\");' style='color:black'>+</button>" + "<ul class='collapse' id='" + ParentIdUl + "'><li><input type='checkbox' name=" + colorsWithCategory[k]["subcategory"] + " id='" + ChildId + "'/><label for=" + ChildId + ">" +  colorsWithCategory[k]["subcategory"] +"</label></li><ul id="+ ParentIdUl + "></ul></li>";
            }
        }
    
    
    var selectedProperties = [];
  	
  	$('input[type="checkbox"]').change(function(e) {

	  var checked = $(this).prop("checked"),
	      container = $(this).parent(),
	      siblings = container.siblings();
	
	  container.find('input[type="checkbox"]').prop({
	    indeterminate: false,
	    checked: checked
	  });
	
	  function checkSiblings(el) {
	
	    var parent = el.parent().parent(),
	        all = true;
	
	    el.siblings().each(function() {
	      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
	    });
	
	    if (all && checked) {
	       
	        
	      parent.children('input[type="checkbox"]').prop({
	        indeterminate: false,
	        checked: checked
	      });
	
	      checkSiblings(parent);
	
	    } else if (all && !checked) {
	
	      parent.children('input[type="checkbox"]').prop("checked", checked);
	      parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
	      checkSiblings(parent);
	
	    } else {
	        
	        
	        
	      el.parents("li").children('input[type="checkbox"]').prop({
	        indeterminate: true,
	        checked: false
	      });
	
	    }
	
	  }
	
	  checkSiblings(container);
	});
    });
    
}



//http://stackoverflow.com/questions/2536379/difference-in-months-between-two-dates-in-javascript


function loadDataOld() {
    //console.log("hello");
    var fadingStartDate = 5;
    for(var i = 0; i < monthDiffApp(startDateForPoints, endDateForPoints); i++)   
            pointsList.push(new Array());

    var colorsWithCategory = [];
    var num_to_load = visdata.length;
    for (var i = 0; i < num_to_load; i++) {
        try{
            var long = visdata[i].Longitude;
            var lat = visdata[i].Latitude;
    
            if (long == "NaN" || lat == "NaN")
            {
                continue;
            }
            var cat = visdata[i].Category;
            var sub_cat = visdata[i].SubCategory.toUpperCase();		// for some reason the sub cats are in upper case!??!
            var rgb = colors[cat][sub_cat];
            var cesiumColor = new Cesium.Color(rgb['r'], rgb['g'], rgb['b'], 0);
            var date = Cesium.JulianDate.fromIso8601(visdata[i].Date);
            var point = new Cesium.Entity(
            {
                name: cat + " - " + sub_cat,
                position : Cesium.Cartesian3.fromDegrees(long, lat),
                //Set the entity availability to the same interval as the simulation time.
                availability : new Cesium.TimeIntervalCollection([new Cesium.TimeInterval({
                    start : Cesium.JulianDate.addDays(date,-fadingStartDate,new Cesium.JulianDate()),
                    stop : Cesium.JulianDate.addDays(date, fadingStartDate, new Cesium.JulianDate())
                })]),
                point : {
                    pixelSize : 5,
                    color : cesiumColor,
                    outlineColor : cesiumColor,
                    outlineWidth : 2
                }
            });
            
            
            var pointWithDay = {date: date ,point:point, startDate: Cesium.JulianDate.addDays(date,-fadingStartDate,new Cesium.JulianDate()), endDate: Cesium.JulianDate.addDays(date, fadingStartDate, new Cesium.JulianDate()), category: cat, subcategory : sub_cat};
            var ds = new Date(visdata[i].Date);
            ds.setDate(ds.getDate() - fadingStartDate);
            pointsList[Math.abs(monthDiffApp(startDateForPoints,ds ))].push(pointWithDay);  
            var newColor = true;
            for(var j = 0; j < colorsWithCategory.length; j++){
                if(colorsWithCategory[j]["color"] == rgb)
                {
                    newColor = false;
                    break;
                }
            }
            
            if(newColor == true || i == 0)
            {
                var newColorCat = {color : rgb, category : cat, subcategory : sub_cat};
                colorsWithCategory.push(newColorCat);
            }
            
        } catch(err){
            console.log(err);
        }
    }

    $(document).ready(function(){
        var t = $('#KeyTable').DataTable();
        addedCategoriesFilt.push({Category : 'none', SubCategory : ["none"] });
        for(var k = 0; k < colorsWithCategory.length; k++)
        {
            t.row.add( [
            colorsWithCategory[k]["category"],
            colorsWithCategory[k]["subcategory"],
            "<div style='background-color:rgb("+colorsWithCategory[k]["color"]["r"]*100 +"%,"+colorsWithCategory[k]["color"]["g"] * 100 +"%,"+colorsWithCategory[k]["color"]["b"] * 100 +"% '>_</div>"
        ] ).draw( false );
            //document.getElementById("catSelect").innerHTML += "<option Value=" + colorsWithCategory[k]["category"] + ">" + colorsWithCategory[k]["category"] + "</option>";    
            //document.getElementById("subcatSelect").innerHTML += "<option Value=" + colorsWithCategory[k]["subcategory"] + ">" + colorsWithCategory[k]["subcategory"] + "</option>";    
            
            var AddedCat = false;
            for(var l = 0; l < addedCategoriesFilt.length; l++)
            {
                if(addedCategoriesFilt[l]['Category'].includes(colorsWithCategory[k]["category"]))
                {
                    AddedCat = true;
                    var AddedSub = false;
                    for(var r = 0; r < addedCategoriesFilt[l]['SubCategory'].length; r++)
                    {
                        if(addedCategoriesFilt[l]['SubCategory'][r].includes(colorsWithCategory[k]["subcategory"]) )
                        {
                            AddedSub = true;
                            break;
                        }
                    }
                    if(AddedSub == false)
                    {
                        addedCategoriesFilt[l]['SubCategory'].push(colorsWithCategory[k]["subcategory"] );
                        var id = "cat-" +  l + "-sub-" + (addedCategoriesFilt[l]['SubCategory'].length - 1);
                        var ParentId =  "cat-" +  l + "-ul";
                        document.getElementById(ParentId).innerHTML += "<li><input type='checkbox' name=" + colorsWithCategory[k]["subcategory"] + " id='" + id + "'/><label for='" + id + "'>" +  colorsWithCategory[k]["subcategory"] +"</label></li>";
                        break;
                    }
                }
                
            }
              if(AddedCat == false)
                {
                    var item = { Category : '', SubCategory : []};
                    item['Category'] = colorsWithCategory[k]["category"];
                    item['SubCategory'] = [(colorsWithCategory[k]["subcategory"] )];
                    addedCategoriesFilt.push(item);
                    var ParentId =  "cat-" +  l;
                    var ChildId = "cat-" +  l + "-sub-0"; 
                    var ParentIdUl =  "cat-" +  l + "-ul";
                    document.getElementById("checkBoxDisplay").innerHTML += "<li> <input type='checkbox' name=" + colorsWithCategory[k]["category"] + " id='" + ParentId + "'/><label for='" + id + "'>" +  colorsWithCategory[k]["category"] +"</label>"+" <button id='" + ParentIdUl + 'But'  + "' onclick='toggle(\"#" + ParentIdUl + "\", \"#" + ParentIdUl + 'But'  + "\");' style='color:black'>+</button>" + "<ul class='collapse' id='" + ParentIdUl + "'><li><input type='checkbox' name=" + colorsWithCategory[k]["subcategory"] + " id='" + ChildId + "'/><label for=" + ChildId + ">" +  colorsWithCategory[k]["subcategory"] +"</label></li><ul id="+ ParentIdUl + "></ul></li>";
                }
        }
    
    
    var selectedProperties = [];
  	
  	$('input[type="checkbox"]').change(function(e) {

	  var checked = $(this).prop("checked"),
	      container = $(this).parent(),
	      siblings = container.siblings();
	
	  container.find('input[type="checkbox"]').prop({
	    indeterminate: false,
	    checked: checked
	  });
	
	  function checkSiblings(el) {
	
	    var parent = el.parent().parent(),
	        all = true;
	
	    el.siblings().each(function() {
	      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
	    });
	
	    if (all && checked) {
	       
	        
	      parent.children('input[type="checkbox"]').prop({
	        indeterminate: false,
	        checked: checked
	      });
	
	      checkSiblings(parent);
	
	    } else if (all && !checked) {
	
	      parent.children('input[type="checkbox"]').prop("checked", checked);
	      parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
	      checkSiblings(parent);
	
	    } else {
	        
	        
	        
	      el.parents("li").children('input[type="checkbox"]').prop({
	        indeterminate: true,
	        checked: false
	      });
	
	    }
	
	  }
	
	  checkSiblings(container);
	});
    });
    

    
}



function toggle(id, thisBut){
    if($(id).attr('class') == "collapse in")
    {
        $(id).attr('class',"collapse");
        $(thisBut).text('+');

    } else if($(id).attr('class') == "collapse")
    {
        $(id).attr('class', "collapse in");
        $(thisBut).text ('-');
    }
}

function initCesium(){
    Cesium.BingMapsApi.defaultKey = 'AuE-QvFcvj-b46lVJZfJtGNsPTOAU_4v7e16-Xz2NNeFs1yM-bbG4x0LJa7VN0s_';

    var start = Cesium.JulianDate.fromIso8601("2004-01-01");
    var stop = Cesium.JulianDate.fromIso8601("2013-01-01");

    //Make sure viewer is at the desired time.
    cesiumWidget.clock.startTime = start;
    cesiumWidget.clock.stopTime = stop.clone();
    cesiumWidget.clock.currentTime = start.clone();
    cesiumWidget.clock.clockRange = Cesium.ClockRange.LOOP_STOP; //Loop at the end
    cesiumWidget.clock.multiplier = 0;
    cesiumWidget.clock.ClockStep = Cesium.ClockStep.TICK_DEPENDENT;
    //Set timeline to simulation bounds
    cesiumWidget.timeline.zoomTo(start, stop);
}