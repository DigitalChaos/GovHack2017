eqfeed_callback({
  "type": "FeatureCollection", 
  "features": [
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.069472, 
          -35.238285
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.069472, 
          -35.238285
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.130584, 
          -35.281054
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.130584, 
          -35.281054
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.139488, 
          -35.249387
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.139488, 
          -35.249387
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.09636, 
          -35.404184
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.09636, 
          -35.404184
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.132515, 
          -35.185072
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.132515, 
          -35.185072
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.020044, 
          -35.221879
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.020044, 
          -35.221879
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.140739, 
          -35.315484
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.140739, 
          -35.315484
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.068624, 
          -35.414752
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.068624, 
          -35.414752
        ]
      }
    }, 
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          149.083925, 
          -35.344378
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "type": "Point", 
        "coordinates": [
          149.083925, 
          -35.344378
        ]
      }
    }
  ]
})