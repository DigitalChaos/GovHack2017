# Visualisation

### Installation

- clone repository
- run the following in the visualisation directory (requires [nodejs](https://nodejs.org/en/))
```
    npm install
    node server.js
```
- Browse to [http://localhost:8080](http://localhost:8080)