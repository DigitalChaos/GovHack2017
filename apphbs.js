const path = require('path');
const express = require('express');
const exphbs = require('express-handlebars');
const mongoose = require('mongoose');

const dir = 'public';
const PORT = 8081;

// connect to the AWS MongoDB

var postCode;

const db = mongoose.connect(
    "mongodb://userdb:HowDoIDoThis@13.210.69.247/govHack2017", {
        useMongoClient: true
    },
    function(error, db) {
        if (error != null) {
            return console.log("Error on connect" + error);
        }
        console.log("Connected to mongodb");
        db.collection("PostCode").find({}).toArray(function(err, res) {
            if (err) {
                throw err;
            }
            postCode = res;
            db.close();
        });
    }
);

const app = express();

app.locals.basedir = dir;

app.set('view engine', '.html');
app.set('views', path.join(dir, 'views'));
console.log("views dir: " + app.get('views'));
app.engine('.html', exphbs({
    defaultLayout: 'main',
    extname: '.html',
    layoutsDir: path.join(app.get('views'), 'layouts')
}));

app.get('/', (req, res) => {
    res.render('home');
});

app.get("/views/*", (req, res) => {
    console.log("Query: " + JSON.stringify(req.query));
    if (JSON.stringify(req.query) == "{}") {
        console.log("No query data, loading page as normal.");
        console.log("Request: " + req.path + ". Rendering page: " + req.path.substring(7));
        res.render(req.path.substring(7));
    }
    else {
        var polyData = [];
        postCode.forEach((d) => {
            if (Array.isArray(d.shape))
                d.shape.forEach((p) => polyData.push(p));
            else
                polyData.push(d.shape);
        });
        res.send(JSON.stringify(polyData));
        res.end();
    }
});

app.listen(PORT, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }
    console.log('Development server running publicly. Connect to %s:%d ', process.env.IP, PORT);
});

app.use((err, request, response, next) => {
    console.log(err);
    response.status(500).send('Something broke!\n');
});

app.use(express.static(dir)); // has to be done at the end cause it fucks shit up
