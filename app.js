(function() {
    // "use strict"; // da fuq is this

    const express = require('express');
    const pug = require('pug');
    var mongoose = require('mongoose');
    // const stylus = require('stylus');

    const app = express();

    app.set('views', 'public/views');
    app.set('view engine', 'pug');
    app.set('title', 'Gov Hack 2017');
    app.use(express.static(__dirname + '/public'));
    app.locals.basedir = __dirname + '/public';

    app.get('/', (req, res) => res.render('index', {
        title: app.get('title'),
        message: 'Hello there!',
        youAreUsingPug: true
    }));

    const port = process.env.PORT
    const ip = process.env.IP;

    const server = app.listen(port, ip, () => console.log(app.get('title') + ' development server running publicly. Connect to %s:%d/', ip, port));

    server.on('error', function(e) {
        if (e.code === 'EADDRINUSE') {
            console.log('Error: Port %d is already in use, select a different port.', port);
            console.log('Example: node app.js --port %d', port + 1);
        }
        else if (e.code === 'EACCES') {
            console.log('Error: This process does not have permission to listen on port %d.', port);
            if (port < 1024) {
                console.log('Try a port number higher than 1024.');
            }
        }
        console.log(e);
        process.exit(1);
    });

    server.on('close', function() {
        console.log(app.get('title') + ' development server stopped.');
    });

    process.on('SIGINT', function() {
        server.close(function() {
            process.exit(0);
        });
    });

    // connect to the AWS MongoDB
    var db = mongoose.connect(
        "mongodb://userdb:HowDoIDoThis@13.210.69.247/govHack2017", {
            useMongoClient: true
        },
        function(error) {
            if (error != null) {
                // Check error in initial connection. There is no 2nd param to the callback.
                console.log("Error on connect" + error);
            }
            else {
                console.log("Connected to mongodb");
            }
        }
    );

})();
