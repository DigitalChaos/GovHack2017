/* global google */
/* global $ */
/* global addPolygon */

/**
 * Loads the data from the provided kml file. This is an async operation.
 * @param kml The link/url to the kml file. Ex: "data/AustralianPostcodesSimplified.kml"
 * @param map The map to display the loaded kml poly data.
 */
function loadKmlData(kml, map) {
  var a = kml.split('.');
  if (a.length > 1 && a[1] != "kml") { // check file extension
    alert("Invalid file extension. '.kml' required.\n" + kml);
    return;
  }
  else if (a.length == 1) { // no file extension, add .kml
    kml = kml + ".kml";
  }
  console.log("Attempting to load kml: " + kml);
  $.ajax({
    type: "GET",
    url: kml,
    dataType: "xml",
    success: function(xml) {
      $(xml).find("coordinates").each((i, e) => addPolygon(map, readCoords(e.innerHTML)));
    },
    error: function(e) {
      alert("An error occurred while processing XML file.");
      console.log(e);
    }
  });
}

/**
 * Reads the coordinates from the text as a list of google map Lat Long coordinates.
 * 
 * @param text The text containing the desired coordinates.
 * @return A list of google map coordinates.
 */
function readCoords(text) {
  // read polygon coordinates
  var coords = [];
  text.replace(new RegExp("([^0-9.,-])+", 'g'), ' ').split(' ').forEach(function(s) {
    s = s.trim();
    if (s != null && s.length > 0) {
      var a = s.split(',');
      coords.push({
        lat: parseFloat(a[1]), // a[1] is lat
        lng: parseFloat(a[0]) // a[0] is long
      });
    }
  });
  return coords;
}
