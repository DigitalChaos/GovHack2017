/* global google */
/* global $ */

var map;
var dataVal;
var comData;
var markers = new Array();

const addr = "https://govhack2017-digitalchaos007.c9users.io:8080/data/";

const imgaddr = "https://govhack2017-digitalchaos007.c9users.io:8080/images/";

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {
      lat: -34.397,
      lng: 150.644
    },
    //   mapTypeId: 'terrain'
  });
  // loadData();
  loadKml(addr + "ACT_Division_Boundaries.kml");
}

function find_in_object(my_object, my_criteria) {

  return my_object.filter(function(obj) {
    return Object.keys(my_criteria).every(function(c) {
      return obj[c] == my_criteria[c];
    });
  });

}

function search() {
  //'SELECT * FROM "54566d76-a809-4959-8622-61dc30b3114d" WHERE "State" LIKE' + "'Queensland'" // query for 'jones';

  clearAllMarkers()
  clearAllKML()

  $('input[name=filter]').each(function() {
    if (this.checked) {
      var rad = $(this).closest('td').next('td').next('td').find('input').val();
      if (this.value == 'bus')
        loadKml(addr + 'Bus_Stops_point_locations.kml');
      if (this.value == 'toilet') {
        drawMarkers('"54566d76-a809-4959-8622-61dc30b3114d"')
      }
      if (this.value == 'skate') {
        loadjson('https://www.data.act.gov.au/resource/5yva-w7wu.json', 'latitude', 'longitude', '', 'SkatePark.png', rad, '#ef2753')
      }
      if (this.value == 'lib')
        loadjson('https://www.data.act.gov.au/resource/hd64-stha.json', '', '', 'lib', 'library.png', rad, '#e09c31')

      if (this.value == 'hos')
        loadjson('https://www.data.act.gov.au/resource/ad3m-fjew.json', '', '', 'hos', 'Hospital.png', rad, '#f45e41')

      if (this.value == 'suburb')
        loadKml(addr + 'ACT_Division_Boundaries.kml');

      if (this.value == "popo")
        loadjson('https://www.data.act.gov.au/resource/g2t2-ix3i.json', '', '', 'popo', 'police_station.png', rad, '#283cef')

      if (this.value == "preschool") {
        //We do that to ensure to get a correct JSON

        $.getJSON('https://www.data.act.gov.au/resource/kryz-yqb4.json', function(data) {
          //We can use {'name': 'Lenovo Thinkpad 41A429ff8'} as criteria too
          var my_json = JSON.stringify(data)

          var filtered_json = $.grep(JSON.parse(my_json), function(element, index) {
            if (element.type == 'Preschool to Year 10') return element;
          });
          console.log(filtered_json);
          drawMapIcons(filtered_json, '', '', 'preschool', 'Schools.png', rad, '#a8b21c')

        });

      }

      if (this.value == "primary") {
        $.getJSON('https://www.data.act.gov.au/resource/kryz-yqb4.json', function(data) {
          //We can use {'name': 'Lenovo Thinkpad 41A429ff8'} as criteria too
          var my_json = JSON.stringify(data)

          var filtered_json = $.grep(JSON.parse(my_json), function(element, index) {
            if (element.type == 'Primary School') return element;
          });
          console.log(filtered_json);
          drawMapIcons(filtered_json, '', '', 'preschool', 'primary.png', rad, '#a8b21c')

        });
      }

      if (this.value == "high") {
        $.getJSON('https://www.data.act.gov.au/resource/kryz-yqb4.json', function(data) {
          //We can use {'name': 'Lenovo Thinkpad 41A429ff8'} as criteria too
          var my_json = JSON.stringify(data)

          var filtered_json = $.grep(JSON.parse(my_json), function(element, index) {
            if (element.type == 'High School') return element;
          });
          console.log(filtered_json);
          drawMapIcons(filtered_json, '', '', 'preschool', 'highschool.png', rad, '#a8b21c')

        });
      }

      if (this.value == "college") {

        $.getJSON('https://www.data.act.gov.au/resource/kryz-yqb4.json', function(data) {
          //We can use {'name': 'Lenovo Thinkpad 41A429ff8'} as criteria too
          var my_json = JSON.stringify(data)

          var filtered_json = $.grep(JSON.parse(my_json), function(element, index) {
            if (element.type == 'Colleges Year 11-12') return element;
          });
          console.log(filtered_json);
          drawMapIcons(filtered_json, '', '', 'preschool', 'college.png', rad, '#a8b21c')

        });
      }
    }
    else {}
  });
}

function drawMarkers(db) {
  /*
  if ($("#locTypeSelect").val() != 'Postcode')
    var query = 'SELECT * FROM ' + db + ' WHERE  "' + $("#locTypeSelect").val() + '" LIKE \'' + $("#locationTextField").val() + '\'';
  else
    var query = 'SELECT * FROM "54566d76-a809-4959-8622-61dc30b3114d" WHERE  "' + $("#locTypeSelect").val() + '" = \'' + parseInt($("#locationTextField").val()) + '\'';
  */

  var query = 'SELECT * FROM ' + db + ' WHERE  "State" LIKE \'' + 'Australian Capital Territory' + '\'';
  loadData(query);

}

function clearAllMarkers() {
  for (var i = 0; i < markers.length; i++)
    markers[i].setMap(null);

  markers = new Array();
}

function clearAllKML() {
  kmlLayerArray.forEach(function(item) {
    item.setMap(null);
  });
  kmlLayerArray = new Array();

}

var kmlLayerArray = new Array();

function filtering() {


}
/*
function loadjson()
{
  $.ajax({
    url: "https://www.data.act.gov.au/resource/hd64-stha.json",
    type: "GET",
    data: {
      "$limit" : 5000,
    }
}).done(function(data) {
  alert("Retrieved " + data.length + " records from the dataset!");
  console.log(data);
});

}
*/
function loadKml(kml) // String - kml source location
{
  var icon = {
    url: imgaddr + 'bus-driver.png', // url
    scaledSize: new google.maps.Size(30, 30), // scaled size
  };

  var kmlLayer = new google.maps.KmlLayer(kml, {
    suppressInfoWindows: false,
    preserveViewport: false,
    map: map,
  });


  console.log(kmlLayer);

  for (var i; i < kmlLayer.length; i++)
    console.log(kmlLayer[i]);

  kmlLayerArray.push(kmlLayer);

  // console.log(kmlLayer);

  // kmlLayer.addListener('click', function(kmlEvent) {
  //   console.log(kmlEvent);
  //   var text = kmlEvent.featureData.infoWindowHtml;
  //   var sidediv = document.getElementById('content-window');
  //   sidediv.innerHTML = text;
  // });

}


function loadjson(jsonLoc, latstr, longstr, datatype, iconImg, radius, colourCircle) {
  $.getJSON(jsonLoc, function(data) {
    var arr = data;
    drawMapIcons(arr, latstr, longstr, datatype, iconImg, radius, colourCircle);

  });
}

function loadData(query) {
  dataVal = {
    sql: query
  };
  console.log(dataVal);
  $.ajax({
    url: 'https://data.gov.au/api/action/datastore_search_sql',
    data: dataVal,
    success: function(data) {
      console.log(data.result);
      comData = data.result.records;
      drawMapIcons(comData, 'Latitude', 'Longitude', 'toilet', 'dunny.png', '', '');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      console.log(XMLHttpRequest);
      console.log("Error: " + errorThrown);
    }
  });
}

function drawMapIcons(arr, latstr, longstr, datatype, iconImg, radius, circleColour) {
  if (arr == null)
    var data = comData.result.records;
  else
    var data = arr;

  console.log(data);

  if (latstr == null || longstr == null) {
    latstr = 'latitude';
    longstr = 'longitude';
  }

  var icon = {
    url: imgaddr + iconImg, // url
    scaledSize: new google.maps.Size(30, 30), // scaled size
  };
  var arrMarkers = new Array();

  for (var i = 0; i < data.length; i++) {
    var pos;
    try {
      if (datatype == 'lib' || datatype == 'hos' || datatype == 'popo' || datatype == 'preschool')
        pos = new google.maps.LatLng(parseFloat(data[i]['location_1']['coordinates'][1]), parseFloat(data[i]['location_1']['coordinates'][0]));
      else
        pos = new google.maps.LatLng(parseFloat(data[i][latstr]), parseFloat(data[i][longstr]));

      var marker = new google.maps.Marker({
        position: pos,
        map: map,
        icon: icon,
        title: 'Toilet'
      });
      markers.push(marker);

      //arrMarkers.push(marker);
      /*google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {*/
      if (radius != '' || radius != '0') {
        var circle = new google.maps.Circle({
          strokeColor: circleColour,
          strokeOpacity: 0.8,
          strokeWeight: 1.5,
          fillColor: circleColour,
          fillOpacity: 0.35,
          map: map,
          center: marker.getPosition(),
          radius: parseFloat(radius) * 1000
        });
        markers.push(circle);
      }
      //markers.push(circle);                  
      /*}
      })(marker, i));*/
    }
    catch (err) {
      console.log(err);
    }
  }
}

function bermudaTriangle() {
  // Define the LatLng coordinates for the polygon's path.
  var triangleCoords = [{
    lat: 25.774,
    lng: -80.190
  }, {
    lat: 18.466,
    lng: -66.118
  }, {
    lat: 32.321,
    lng: -64.757
  }, {
    lat: 25.774,
    lng: -80.190
  }];

  // Construct the polygon.
  var bermudaTriangle = new google.maps.Polygon({
    paths: triangleCoords,
    strokeColor: '#FF0000',
    strokeOpacity: 0.8,
    strokeWeight: 2,
    fillColor: '#FF0000',
    fillOpacity: 0.35
  });

  bermudaTriangle.setMap(map);
}

//Toggles nav bar search field

$('#searchDropDown').on('click', function(event) {
  $(this).parent().toggleClass('open');
});
