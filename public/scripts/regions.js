/* global google */
/* global $ */

const polyColor = 0xFFFF0000;
const polyHover = 0xFF660000;

/**
 * Draws and adds a polygon to the provided map using the provided coords.
 * 
 * @param map The map to add the polygon.
 * @param coords The coordinates to use as the polygon path.
 */
function addPolygon(map, coords) {
    // Construct the polygon.
    var p = new google.maps.Polygon({
        paths: coords,
        strokeColor: toColor(polyColor),
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: toColor(polyColor),
        fillOpacity: 0.35
    });
    p.setMap(map);

    // Add a listeners for hover events
    google.maps.event.addListener(p, 'mouseover', function(event) {
        var c = toColor(polyColor & polyHover);
        this.setOptions({
            strokeColor: c,
            fillColor: c
        });
    });
    google.maps.event.addListener(p, 'mouseout', function(event) {
        var c = toColor(polyColor);
        this.setOptions({
            strokeColor: c,
            fillColor: c
        });
    });
}

function toColor(num) {
    num >>>= 0;
    var b = num & 0xFF,
        g = (num & 0xFF00) >>> 8,
        r = (num & 0xFF0000) >>> 16,
        a = ((num & 0xFF000000) >>> 24) / 255;
    return "rgba(" + [r, g, b, a].join(",") + ")";
}
