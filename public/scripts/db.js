/* global google */
/* global $ */
/* global addPolygon */

/**
 * Loads the data from the provided kml file. This is an async operation.
 * @param kml The link/url to the kml file. Ex: "data/AustralianPostcodesSimplified.kml"
 * @param map The map to display the loaded kml poly data.
 */
function loadData(map) {
  console.log("Attempting to load data.");
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "?id=1",
    success: function(data) {
      data.forEach((d) => {
        addPolygon(map, d);
      });
    },
    error: function(e) {
      alert("An error occurred while requesting data.");
      console.log(e);
    }
  });
}

/**
 * Reads the coordinates from the 
 * 
 * @param text The text containing the desired coordinates.
 * @return A list of google map coordinates.
 */
function readCoords(text) {
  // read polygon coordinates
  var coords = [];

  return coords;
}
